# README #

My Cards is an iOS application to manage your credit/debit cards as well as bank/booth information. iOS 7+ is supported.

### Screenshots ###

![My Cards Screenshots](https://bytebucket.org/sharafat_8271/mycards/raw/ec2b126a9cd7ef8cd4e57412bf03f12e744b11f3/Screenshots.png)