//
//  CardWebService.m
//  MyCards
//
//  Created by Abdullah Al Mamun on 5/29/14.
//  Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import "CardWebService.h"
#import "RestKitUtil.h"
#import "RKObjectRequestOperation.h"
#import "Card.h"
#import <RestKit/RestKit.h>

@implementation CardWebService

- (void)retrieveCardListAndDo:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))successBlock {
    [[RKObjectManager sharedManager] getObjectsAtPath:[RestKitUtil urlForKey:@"cardsUrl"]
                                           parameters:nil
                                              success:successBlock
                                              failure:^(RKObjectRequestOperation *operation, NSError *error) {

        NSLog(@"Error on retrieving cardList: %@", [error description]);

        [[[UIAlertView alloc] initWithTitle:@"Error"
                                    message:[error description]
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }];
}

- (void)saveCard:(Card *)card
  andDoOnSuccess:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))successBlock
    andOnFailure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failureBlock {

    [[RKObjectManager sharedManager] postObject:card
                                           path:[RestKitUtil urlForKey:@"cardUrl"]
                                     parameters:nil
                                        success:successBlock
                                        failure:failureBlock];
}

@end
