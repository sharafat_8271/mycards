//
//  CardWebService.h
//  MyCards
//
//  Created by Abdullah Al Mamun on 5/29/14.
//  Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RKObjectRequestOperation;
@class RKMappingResult;
@class Card;

@interface CardWebService : NSObject

- (void)retrieveCardListAndDo:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))successBlock;

- (void)saveCard:(Card *)card
  andDoOnSuccess:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))successBlock
    andOnFailure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failureBlock;

@end
