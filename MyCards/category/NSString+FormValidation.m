//
// Created by Arefeen on 5/29/14.
// Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import "NSString+FormValidation.h"


@implementation NSString (FormValidation)

- (BOOL)isValidBankName {
    NSString *regex = @"[A-Za-z ]+";
    NSPredicate *bankNamePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [bankNamePredicate evaluateWithObject:self];
}

- (BOOL)isValidCardNumber {
    NSString *regex = @"[0-9 ]+";
    NSPredicate *cardNumberPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [cardNumberPredicate evaluateWithObject:self];
}

- (BOOL)isValidCardHolderName {
    NSString *regex = @"[A-Za-z ]+";
    NSPredicate *cardHolderPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [cardHolderPredicate evaluateWithObject:self];
}

- (BOOL)isValidMonth {
    return (self.intValue >= 1 && self.intValue <= 12);
}

- (BOOL)isValidYear {
    return (self.intValue >= 1 && self.intValue <= 99);
}

- (BOOL)isEarlierYear:(NSString *)year {
    return (self.intValue < year.intValue);
}

@end