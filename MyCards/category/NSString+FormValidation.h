//
// Created by Arefeen on 5/29/14.
// Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString (FormValidation)

- (BOOL)isValidBankName;

- (BOOL)isValidCardNumber;

- (BOOL)isValidCardHolderName;

- (BOOL)isValidMonth;

- (BOOL)isValidYear;

- (BOOL)isEarlierYear:(NSString *)year;

@end