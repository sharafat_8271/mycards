//
// Created by Sharafat Ibn Mollah Mosharraf on 5/21/14.
// Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Card : NSObject

typedef NS_ENUM(NSInteger, CardType) {
    CardTypeVisa,
    CardTypeMaster,
    CardTypeAmericanExpress,
    CardTypeOther
};

@property (nonatomic, copy) NSString *cardNumber;
@property (nonatomic, copy) NSString *issuerBank;
@property (nonatomic, copy) NSString *cardHolderName;
@property (nonatomic, copy) NSString *validFromMonth;
@property (nonatomic, copy) NSString *validFromYear;
@property (nonatomic, copy) NSString *validUntilMonth;
@property (nonatomic, copy) NSString *validUntilYear;
@property (nonatomic, copy) NSString *cvv;
@property (nonatomic, copy) NSNumber *bankContactId;
@property (nonatomic, assign) CardType cardType;

@end
