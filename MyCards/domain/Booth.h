//
// Created by Mushfek on 5/28/14.
// Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>


@interface Booth : NSObject <MKAnnotation>

@property (nonatomic, copy) NSString *boothName;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;

- (id)initWithName:(NSString *)boothName address:(NSString *)address coordinate:(CLLocationCoordinate2D)coordinate;

@end