//
// Created by Mushfek on 5/28/14.
// Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import "Booth.h"

@implementation Booth

- (id)initWithName:(NSString *)boothName address:(NSString *)address coordinate:(CLLocationCoordinate2D)coordinate {
    if (self = [super init]) {
        if ([boothName isKindOfClass:[NSString class]]) {
            self.boothName = boothName;
        } else {
            self.boothName = @"ATM Booth";
        }

        self.address = address;
        self.coordinate = coordinate;
    }

    return self;
}

@end