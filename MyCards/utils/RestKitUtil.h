//
// Created by Sharafat Ibn Mollah Mosharraf on 5/29/14.
// Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RKObjectMapping;
@class RKResponseDescriptor;


@interface RestKitUtil : NSObject

+ (void)configureRestKit;

+ (NSString *)urlForKey:(NSString *)key;

+ (NSDictionary *)jsonMapForClass:(Class)clazz;

+ (RKResponseDescriptor *)responseDescriptorForError;

@end
