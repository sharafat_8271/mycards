//
// Created by Sharafat Ibn Mollah Mosharraf on 6/1/14.
// Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import "RestKitDictionaryContainer.h"


@implementation RestKitDictionaryContainer

- (id)init {
    self = [super init];
    if (self) {
        _dictionary = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    [_dictionary setObject:value forKey:key];
}

- (id)valueForUndefinedKey:(NSString *)key {
    return [_dictionary objectForKey:key];
}

@end
