//
// Created by Sharafat Ibn Mollah Mosharraf on 5/29/14.
// Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import <RestKit/ObjectMapping/RKErrorMessage.h>
#import <RestKit/Network/RKResponseDescriptor.h>
#import <RestKit/Network/RKRequestDescriptor.h>
#import "RestKitUtil.h"
#import "AFHTTPClient.h"
#import "RKObjectManager.h"
#import "ClassPropertyUtil.h"
#import "NSString+NSStringExtension.h"
#import "Card.h"
#import "RestKitDictionaryContainer.h"

static NSDictionary *dictRoot = nil;

@implementation RestKitUtil

+ (NSString *)urlForKey:(NSString *)key {
    NSString *basePath = [dictRoot valueForKey:@"basePath"];
    return concat(basePath, [dictRoot valueForKey:key]);
}

+ (void)configureRestKit {
    dictRoot = [NSDictionary dictionaryWithContentsOfFile:
            [[NSBundle mainBundle] pathForResource:@"WebServiceURLs" ofType:@"plist"]];

    NSURL *baseURL = [NSURL URLWithString:[dictRoot objectForKey:@"domain"]];
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    RKObjectManager *objectManager = [[RKObjectManager alloc] initWithHTTPClient:client];
    [RKObjectManager setSharedManager:objectManager];

    [self addRequestDescriptors];
    [self addResponseDescriptors];
}

+ (void)addRequestDescriptors {
    RKRequestDescriptor *getCardListRequestDescriptor =
            [RKRequestDescriptor requestDescriptorWithMapping:[self jsonRequestMapForClass:[Card class]]
                                                  objectClass:[Card class]
                                                  rootKeyPath:nil
                                                       method:RKRequestMethodPOST];

    [[RKObjectManager sharedManager] addRequestDescriptor:getCardListRequestDescriptor];
}

+ (void)addResponseDescriptors {
    NSIndexSet *successStatusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful);

    RKResponseDescriptor *getCardListResponseDescriptor =
            [RKResponseDescriptor responseDescriptorWithMapping:[self jsonResponseMapForClass:[Card class]]
                                                         method:RKRequestMethodGET
                                                    pathPattern:[self urlForKey:@"cardsUrl"]
                                                        keyPath:nil
                                                    statusCodes:successStatusCodes];

    RKResponseDescriptor *editCardResponseDescriptor =
            [RKResponseDescriptor responseDescriptorWithMapping:[self jsonResponseMapToDictionary]
                                                         method:RKRequestMethodPOST
                                                    pathPattern:[self urlForKey:@"cardUrl"]
                                                        keyPath:nil
                                                    statusCodes:successStatusCodes];

    [[RKObjectManager sharedManager] addResponseDescriptorsFromArray:@[
            [self responseDescriptorForError], getCardListResponseDescriptor, editCardResponseDescriptor]
    ];
}

+ (RKResponseDescriptor *)responseDescriptorForError {
    NSIndexSet *errorStatusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassClientError);
    return [RKResponseDescriptor responseDescriptorWithMapping:[self errorDescMapping]
                                                        method:RKRequestMethodAny
                                                   pathPattern:nil
                                                       keyPath:nil
                                                   statusCodes:errorStatusCodes];
}


+ (RKObjectMapping *)errorDescMapping {
    RKObjectMapping *errorMapping = [RKObjectMapping mappingForClass:[RKErrorMessage class]];
    [errorMapping addPropertyMapping:[RKAttributeMapping attributeMappingFromKeyPath:@"message"
                                                                           toKeyPath:@"errorMessage"]];

    return errorMapping;
}

+ (RKObjectMapping *)jsonRequestMapForClass:(Class)clazz {
    RKObjectMapping *rkObjectMapping = [RKObjectMapping requestMapping];
    [rkObjectMapping addAttributeMappingsFromDictionary:[self jsonMapForClass:clazz]];

    return rkObjectMapping;
}

+ (RKObjectMapping *)jsonResponseMapForClass:(Class)clazz {
    RKObjectMapping *rkObjectMapping = [RKObjectMapping mappingForClass:clazz];
    [rkObjectMapping addAttributeMappingsFromDictionary:[self jsonMapForClass:clazz]];

    return rkObjectMapping;
}

//TODO: Doesn't work... fix this...
+ (RKObjectMapping *)jsonResponseMapToDictionary {
    RKObjectMapping *dictionaryMapping = [RKObjectMapping mappingForClass:[RestKitDictionaryContainer class]];
    [dictionaryMapping addAttributeMappingsFromArray:@[@"success"]];

    return dictionaryMapping;
}

+ (NSDictionary *)jsonMapForClass:(Class)clazz {
    NSDictionary *jsonObjectPropertyMap = [[NSMutableDictionary alloc] init];

    NSDictionary *classProperties = [ClassPropertyUtil classPropertiesFor:clazz];
    for (id classProperty in classProperties) {
        [jsonObjectPropertyMap setValue:classProperty forKey:classProperty];
    }

    return jsonObjectPropertyMap;
}

@end
