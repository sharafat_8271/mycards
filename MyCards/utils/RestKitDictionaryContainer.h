//
// Created by Sharafat Ibn Mollah Mosharraf on 6/1/14.
// Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface RestKitDictionaryContainer : NSObject

@property (nonatomic, strong) NSMutableDictionary * dictionary;

@end
