//
// Created by Sharafat Ibn Mollah Mosharraf on 5/29/14.
// Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
* http://stackoverflow.com/questions/754824/get-an-object-properties-list-in-objective-c#answer-13000074
*/
@interface ClassPropertyUtil : NSObject

+ (NSDictionary *)classPropertiesFor:(Class)klass;

@end
