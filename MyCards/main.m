//
//  main.m
//  MyCards
//
//  Created by Sharafat Ibn Mollah Mosharraf on 5/21/14.
//  Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import "AppDelegate.h"
#import "JSObjection.h"
#import "RestKitUtil.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        [JSObjection setDefaultInjector:[JSObjection createInjector]];
        [RestKitUtil configureRestKit];

        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }

}
