//
// Created by Sharafat Ibn Mollah Mosharraf on 5/21/14.
// Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Card;

@interface CardDao : NSObject

- (NSUInteger)cardCount;

- (Card *)cardAtIndex:(NSUInteger)index;

- (void)retrieveCardListAndDo:(void(^)(void))onSuccess;

- (void)addOrUpdateCard:(Card *)card andDo:(void (^)(BOOL, NSString *, id))onResult;

- (void)deleteCard:(Card *)card;

- (NSMutableArray *)nearestBoothList;

@end
