//
// Created by Sharafat Ibn Mollah Mosharraf on 5/21/14.
// Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <CoreLocation/CoreLocation.h>
#import <RestKit/ObjectMapping/RKMappingResult.h>
#import "CardDao.h"
#import "Card.h"
#import "Objection.h"
#import "AppDelegate.h"
#import "Booth.h"
#import "CardWebService.h"
#import "RKObjectManager.h"
#import "RestKitDictionaryContainer.h"

@interface CardDao () {
    NSManagedObjectContext *managedObjectContext;
}

@property(nonatomic, strong) CardWebService *cardService;
@property(nonatomic, strong) NSMutableArray *cardList;

@end


@implementation CardDao

objection_register_singleton(CardDao)

objection_requires(@"cardService")

- (instancetype)init {
    self = [super init];
    if (self) {
        managedObjectContext = get_managed_object_context();
        _cardList = [[NSMutableArray alloc] init];
    }

    return self;
}

- (void)retrieveCardListAndDo:(void(^)(void))onSuccess {
    //Fetch card from Core Data
    NSFetchRequest *cardFetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Card"];
    self.cardList = [[managedObjectContext executeFetchRequest:cardFetchRequest error:nil] mutableCopy];

    //Fetch card from web service
    [_cardService retrieveCardListAndDo:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        self.cardList = (NSMutableArray *) mappingResult.array;
        onSuccess();
    }];
}

- (NSUInteger)cardCount {
    return [self.cardList count];
}

- (Card *)cardAtIndex:(NSUInteger)index {
    return [self.cardList objectAtIndex:index];
}

- (void)addOrUpdateCard:(Card *)card andDo:(void(^)(BOOL success, NSString *errorMsg, id returnObject))onResult {
    //Save card to core data
    id persistentCard = [self getCardByCardNumber:card];
    if (persistentCard == nil) {
        persistentCard = [NSEntityDescription insertNewObjectForEntityForName:@"Card"
                                                inManagedObjectContext:managedObjectContext];
        [_cardList addObject:persistentCard];
    }

    [persistentCard setValue:card.issuerBank forKey:@"issuerBank"];
    [persistentCard setValue:card.cardNumber forKey:@"cardNumber"];
    [persistentCard setValue:card.cardHolderName forKey:@"cardHolderName"];
    [persistentCard setValue:card.validFromMonth forKey:@"validFromMonth"];
    [persistentCard setValue:card.validFromYear forKey:@"validFromYear"];
    [persistentCard setValue:card.validUntilMonth forKey:@"validUntilMonth"];
    [persistentCard setValue:card.validUntilYear forKey:@"validUntilYear"];
    [persistentCard setValue:card.cvv forKey:@"cvv"];
    [persistentCard setValue:card.bankContactId forKey:@"bankContactId"];
    [persistentCard setValue:[NSNumber numberWithInt:CardTypeVisa] forKey:@"cardType"];

    NSError *error = nil;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Error saving card: %@", error.description);
    }

    //Save card to web service
    [_cardService saveCard:card andDoOnSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        onResult(YES, nil, nil);
    } andOnFailure:^(RKObjectRequestOperation *operation, NSError *error) {
        onResult(NO, [error description], nil);
    }];
}

- (Card *)getCardByCardNumber:(Card *)card {
    return (Card *) [self getManagedCard:card];
}

- (id)getManagedCard:(Card *)card {
    NSError *error = NULL;
    NSMutableArray *cardByIdFetchResults;

    NSFetchRequest *cardByIdFetchRequest = [[NSFetchRequest alloc] init];
    [cardByIdFetchRequest setEntity:[NSEntityDescription entityForName:@"Card" inManagedObjectContext:managedObjectContext]];
    [cardByIdFetchRequest setPredicate:[NSPredicate predicateWithFormat:@"cardNumber == %@", card.cardNumber]];
    cardByIdFetchResults = [[managedObjectContext executeFetchRequest:cardByIdFetchRequest error:&error] mutableCopy];

    if (!cardByIdFetchResults) {
        NSLog(@"Error fetching cardByCardNumber: %@", error.description);
        return nil;
    } else if ([cardByIdFetchResults count] == 0) {
        NSLog(@"No card found by cardNumber: %@", card.cardNumber);
        return nil;
    }

    return [cardByIdFetchResults objectAtIndex:0];
}

- (void)deleteCard:(Card *)card {
    id managedCard = [self getManagedCard:card];
    if (managedCard != nil) {
        NSError *error = NULL;

        [managedObjectContext deleteObject:managedCard];

        BOOL savedSuccessfully = ![managedObjectContext save:&error];
        if (!savedSuccessfully) {
            NSLog(@"Error in saving the context: %@", error);
        }

        [self.cardList removeObject:card];
    }
}

- (NSMutableArray *)nearestBoothList {
    NSMutableArray *boothList = [[NSMutableArray alloc] init];

    CLLocationCoordinate2D boothOneCoordinate;
    boothOneCoordinate.latitude = 23.79290;
    boothOneCoordinate.longitude = 90.401325;
    Booth *boothOne = [[Booth alloc] initWithName:@"HSBC" address:@"43, dkfsldkfjld" coordinate:boothOneCoordinate];
    [boothList addObject:boothOne];

    CLLocationCoordinate2D boothTwoCoordinate;
    boothTwoCoordinate.latitude = 23.792050;
    boothTwoCoordinate.longitude = 90.401275;
    Booth *boothTwo = [[Booth alloc] initWithName:@"DBBL ATM" address:@"08, kldfklsdkf" coordinate:boothTwoCoordinate];
    [boothList addObject:boothTwo];

    CLLocationCoordinate2D boothThreeCoordinate;
    boothThreeCoordinate.latitude = 23.792150;
    boothThreeCoordinate.longitude = 90.401280;
    Booth *boothThree = [[Booth alloc] initWithName:@"Brac Bank ATM" address:@"dsldjlfkd" coordinate:boothThreeCoordinate];
    [boothList addObject:boothThree];

    CLLocationCoordinate2D boothFourCoordinate;
    boothFourCoordinate.latitude = 23.792030;
    boothFourCoordinate.longitude = 90.401270;
    Booth *boothFour = [[Booth alloc] initWithName:@"Standard Chartered" address:@"34, dkfjlsdfjlsf" coordinate:boothFourCoordinate];
    [boothList addObject:boothFour];

    return boothList;
}

@end
