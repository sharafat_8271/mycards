//
// Created by Sharafat Ibn Mollah Mosharraf on 5/25/14.
// Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <Objection/JSObjection.h>
#import "FindBoothViewController.h"
#import "Card.h"
#import "CardDetailsTabBarViewController.h"
#import "CardDao.h"
#import "ObjectionExtension.h"
#import "Booth.h"


@interface FindBoothViewController() {
    CardDao *cardDao;
}
@end

@implementation FindBoothViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _card = ((CardDetailsTabBarViewController *) self.tabBarController).card;

    cardDao = objection_inject(CardDao);
    
//    self.mapView.delegate = self;
//    self.mapView.mapType = MKMapTypeStandard;
//    self.mapView.showsUserLocation = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self configureView];

    [self plotNearestBooths:[cardDao nearestBoothList]];
}

- (void)configureView {
    self.tabBarController.title = @"Locate Booths";
}

- (void)mapView: (MKMapView *)mapView didUpdateUserLocation: (MKUserLocation *)userLocation {
    // Add an annotation in current location
//    MKPointAnnotation *pointAnnotation = [[MKPointAnnotation alloc] init];
//    pointAnnotation.coordinate = userLocation.coordinate;
//    pointAnnotation.title = @"Therap BD Ltd";
//    pointAnnotation.subtitle = @"Road #4, Banani, Dhaka";
//    [self.mapView addAnnotation:pointAnnotation];
    
    [self.mapView setCenterCoordinate:userLocation.coordinate animated:YES];

    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 1000, 1000);
    [self.mapView setRegion:region animated:YES];
}

- (void)plotNearestBooths:(NSMutableArray *)nearestBoothList {
    for (id booth in [nearestBoothList objectEnumerator]) {
        NSString *name = [booth boothName];
        NSString *address = [booth address];
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = [booth coordinate].latitude;
        coordinate.longitude = [booth coordinate].longitude;

        MKPointAnnotation *pointAnnotation = [[MKPointAnnotation alloc] init];
        pointAnnotation.coordinate = coordinate;
        pointAnnotation.title = name;
        pointAnnotation.subtitle = address;

        [self.mapView addAnnotation:pointAnnotation];

    }
}

@end
