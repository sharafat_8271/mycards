//
// Created by Sharafat Ibn Mollah Mosharraf on 5/26/14.
// Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import "CardDetailsTabBarViewController.h"
#import "Card.h"

@interface CardDetailsTabBarViewController ()

@property(strong, nonatomic) UIPopoverController *masterPopoverController;

@end


@implementation CardDetailsTabBarViewController

- (void)setCard:(Card *)card {
    _card = card;

    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }
}


#pragma mark - Split View

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController {
    barButtonItem.title = NSLocalizedString(@"Master", @"Master");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem {
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}

@end
