//
//  CardListViewController.m
//  MyCards
//
//  Created by Sharafat Ibn Mollah Mosharraf on 5/21/14.
//  Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import "CardListViewController.h"
#import "CardDetailViewController.h"
#import "CardListViewTableCell.h"
#import "CardDao.h"
#import "Card.h"
#import "Objection.h"
#import "ObjectionExtension.h"
#import "MBProgressHUD.h"

@interface CardListViewController () {
    CardDao *cardDao;
}
@end

@implementation CardListViewController

- (void)awakeFromNib {
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }

    cardDao = objection_inject(CardDao);

    [super awakeFromNib];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.detailViewController =
            (CardDetailViewController *) [[self.splitViewController.viewControllers lastObject] topViewController];

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        sleep(1);   // To display animation to the user
        [cardDao retrieveCardListAndDo:^{
            [(UITableView *) self.view reloadData];
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            });
        }];

    });
}

- (void)viewWillAppear:(BOOL)animated {
    [(UITableView *) self.view reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [cardDao cardCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CardListViewTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    Card *card = [cardDao cardAtIndex:(NSUInteger) indexPath.row];
    cell.cardNumberLabel.text = card.cardNumber;
    cell.issuerBankLabel.text = card.issuerBank;

    if (card.cardType != CardTypeOther) {
        NSString *cardImageName = @"";
        switch (card.cardType) {
            case CardTypeVisa:
                cardImageName = @"visa";
                break;
            case CardTypeMaster:
                cardImageName = @"master";
                break;
            case CardTypeAmericanExpress:
                cardImageName = @"amex";
                break;
            case CardTypeOther:
                break;
        }

        cell.cardTypeImage.image = [UIImage imageNamed:cardImageName];
    }

    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showCardDetails"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Card *card = [cardDao cardAtIndex:(NSUInteger) indexPath.row];
        [[segue destinationViewController] setCard:card];
    }
}

@end
