//
// Created by Sharafat Ibn Mollah Mosharraf on 5/26/14.
// Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Card;


@interface CardDetailsTabBarViewController : UITabBarController <UISplitViewControllerDelegate>

@property(assign, nonatomic) Card *card;

@end
