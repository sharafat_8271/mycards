//
// Created by Sharafat Ibn Mollah Mosharraf on 5/25/14.
// Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@class Card;


@interface FindBoothViewController : UIViewController <MKMapViewDelegate>

@property (nonatomic, weak) IBOutlet MKMapView *mapView;

@property(assign, nonatomic) Card *card;

@end
