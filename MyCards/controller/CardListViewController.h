//
//  CardListViewController.h
//  MyCards
//
//  Created by Sharafat Ibn Mollah Mosharraf on 5/21/14.
//  Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CardDetailViewController;

@interface CardListViewController : UITableViewController

@property (strong, nonatomic) CardDetailViewController *detailViewController;

@end
