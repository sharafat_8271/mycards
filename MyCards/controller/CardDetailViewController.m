//
//  CardDetailViewController.m
//  MyCards
//
//  Created by Sharafat Ibn Mollah Mosharraf on 5/21/14.
//  Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import "CardDetailViewController.h"
#import "Card.h"
#import "NSObject+NSObjectExtension.h"
#import "NSString+NSStringExtension.h"
#import "CardDetailsTabBarViewController.h"
#import "CardDao.h"
#import "ObjectionExtension.h"
#import "JSObjection.h"


@interface CardDetailViewController () {
    CardDao *cardDao;
}

@end

@implementation CardDetailViewController

- (void)awakeFromNib {
    [super awakeFromNib];
    cardDao = objection_inject(CardDao);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _card = ((CardDetailsTabBarViewController *) self.tabBarController).card;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self configureView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self removeNavBarButtons];
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (_card) {
        self.issuerBankLabel.text = _card.issuerBank;
        _cardNumberLabel.text = _card.cardNumber;
        _cardHolderNameLabel.text = _card.cardHolderName;

        if ([_card.validFromMonth isNotEmpty]) {
            self.validFromLabel.text = concat(_card.validFromMonth, @"/", _card.validFromYear);
        } else {
            self.validFromView.hidden = true;
        }

        if ([_card.validUntilMonth isNotEmpty]) {
            self.validUntilLabel.text = concat(_card.validUntilMonth, @"/", _card.validUntilYear);
        } else {
            self.validUntilView.hidden = true;
        }

        if ([_card.cvv isNotEmpty]) {
            self.cvvLabel.text = _card.cvv;
        } else {
            self.cvvView.hidden = true;
        }

        if (_card.cardType != CardTypeOther) {
            NSString *cardImageName = @"";
            switch (_card.cardType) {
                case CardTypeVisa:
                    cardImageName = @"visa";
                    break;
                case CardTypeMaster:
                    cardImageName = @"master";
                    break;
                case CardTypeAmericanExpress:
                    cardImageName = @"amex";
                    break;
                case CardTypeOther:
                    break;
            }

            _cardTypeImage.image = [UIImage imageNamed:cardImageName];
        }
    }

    self.tabBarController.title = @"Card Details";
    [self addNavBarButtons];
}

- (IBAction)deleteCard:(id)sender {
    [[[UIAlertView alloc] initWithTitle:@"Warning!"
                                message:@"Are you sure you want to delete the card?"
                               delegate:self
                      cancelButtonTitle:@"Cancel"
                      otherButtonTitles:@"Delete Card", nil] show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        NSString *cardNumber = _card.cardNumber;
        [cardDao deleteCard:_card];
        [self notifyCardDeleted:cardNumber];

        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)notifyCardDeleted:(NSString *)cardNumber {
    UILocalNotification *cardDeletedNotification = [[UILocalNotification alloc] init];
    cardDeletedNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:0];
    cardDeletedNotification.alertBody = concat(@"Deleted Card ", cardNumber, @"!");

    [[UIApplication sharedApplication] scheduleLocalNotification:cardDeletedNotification];
}

- (void)addNavBarButtons {
    UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit
                                                                                target:self
                                                                                action:@selector(onEditButtonClicked:)];
    self.tabBarController.navigationItem.rightBarButtonItem = editButton;
}

- (void)removeNavBarButtons {
    self.tabBarController.navigationItem.rightBarButtonItem = nil;
}

- (void)onEditButtonClicked:(id)sender {
    [self performSegueWithIdentifier:@"editCard" sender:sender];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"editCard"]) {
        [[segue destinationViewController] setCard:_card];
    }
}

@end
