//
// Created by Sharafat Ibn Mollah Mosharraf on 5/21/14.
// Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CardListViewTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *cardNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *issuerBankLabel;
@property (weak, nonatomic) IBOutlet UIImageView *cardTypeImage;

@end
