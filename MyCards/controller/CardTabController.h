//
// Created by Sharafat Ibn Mollah Mosharraf on 5/25/14.
// Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CardTabController : UIViewController <UITabBarDelegate> {
    UITabBar *cardTabBar;
    UIViewController *cardDetailViewController;  // view controller of first tab
    UIViewController *findBoothsViewController;  // view controller of second tab
    UIViewController *contactBankViewController;  // view controller of third tab
}

@property (nonatomic, retain) IBOutlet UITabBar *cardTabBar;
@property (nonatomic, retain) UIViewController *cardDetailViewController;
@property (nonatomic, retain) UIViewController *findBoothsViewController;
@property (nonatomic, retain) UIViewController *contactBankViewController;

@end
