//
// Created by Sharafat Ibn Mollah Mosharraf on 5/25/14.
// Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import "CardTabController.h"
#import "CardDetailViewController.h"
#import "FindBoothsViewController.h"
#import "ContactBankViewController.h"


@implementation CardTabController

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    switch (item.tag) {
        case 1:
            if (cardDetailViewController == nil) {
                cardDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"cardDetailsViewController"];
            }
            [self.view addSubview:cardDetailViewController.view];
            [self addChildViewController:cardDetailViewController];
            break;
        case 2:
            if (findBoothsViewController == nil) {
                findBoothsViewController = [[FindBoothsViewController alloc] init];
            }
            [self.view insertSubview:findBoothsViewController.view belowSubview:cardTabBar];
            break;
        case 3:
            if (contactBankViewController == nil) {
                contactBankViewController = [[ContactBankViewController alloc] init];
            }
            [self.view insertSubview:contactBankViewController.view belowSubview:cardTabBar];
            break;
        default:
            NSLog(@"invalid item");
            break;
    }
}

@end
