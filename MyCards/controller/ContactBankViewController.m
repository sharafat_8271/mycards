//
// Created by Sharafat Ibn Mollah Mosharraf on 5/26/14.
// Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import <Objection/JSObjection.h>
#import <AddressBook/AddressBook.h>
#import "ContactBankViewController.h"
#import "Card.h"
#import "CardDetailsTabBarViewController.h"
#import "CardDao.h"
#import "ObjectionExtension.h"
#import "NSObject+NSObjectExtension.h"
#import "NSString+NSStringExtension.h"


@interface ContactBankViewController () {
    CardDao *cardDao;
    NSString *bankContactNumber;
    NSString *bankContactEmail;
    NSString *bankHomepage;
}
@end


@implementation ContactBankViewController

- (void)awakeFromNib {
    [super awakeFromNib];
    cardDao = objection_inject(CardDao);
}

- (void)viewDidLoad {
    [super viewDidLoad];

    _card = ((CardDetailsTabBarViewController *) self.tabBarController).card;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self configureView];
}

- (void)configureView {
    self.tabBarController.title = @"Contact Bank";

    if (_card.bankContactId.intValue == 0) {
        [_contactBankView setHidden:YES];
        [_pickContactView setHidden:NO];

        return;
    }

    BOOL bankContactsPopulated = [self populateBankContacts];
    if (!bankContactsPopulated) {
        return;
    }

    if ([bankContactNumber isNotEmpty]) {
        [_sendSmsButton setTitle:concat(@"Send Message: ", bankContactNumber) forState:UIControlStateNormal];

        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]]) {
            [_callBankButton setTitle:concat(@"Call: ", bankContactNumber) forState:UIControlStateNormal];
        } else {
            _callBankButton.hidden = YES;
        }
    } else {
        _callBankButton.hidden = YES;
        _sendSmsButton.hidden = YES;
    }

    if ([bankContactEmail isNotEmpty]) {
        [_sendEmailButton setTitle:concat(@"Send Email: ", bankContactEmail) forState:UIControlStateNormal];
    } else {
        _sendEmailButton.hidden = YES;
    }

    if ([bankHomepage isNotEmpty]) {
        [_openHomepageButton setTitle:concat(@"Visit Site: ", bankHomepage) forState:UIControlStateNormal];
    } else {
        _openHomepageButton.hidden = YES;
    }

    [_contactBankView setHidden:NO];
    [_pickContactView setHidden:YES];
}

- (BOOL)populateBankContacts {
    CFErrorRef error = NULL;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &error);

    if (addressBook == nil) {
        NSLog(@"Couldn't create address book. Error: %@", error);
        return FALSE;
    }

    ABRecordRef bankRecord = ABAddressBookGetPersonWithRecordID(addressBook, (ABRecordID) [_card.bankContactId intValue]);
    bankContactNumber = [self getValue:kABPersonPhoneProperty fromRecord:bankRecord];
    bankContactEmail = [self getValue:kABPersonEmailProperty fromRecord:bankRecord];
    bankHomepage = [self getValue:kABPersonURLProperty fromRecord:bankRecord];

    return TRUE;
}

- (NSString *)getValue:(ABPropertyID)propertyId fromRecord:(ABRecordRef)recordRef {
    ABMultiValueRef values = ABRecordCopyValue(recordRef, kABPersonURLProperty);
    for (int i = 0; i < ABMultiValueGetCount(values); i++) {
        NSString *value = (__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(values, i);
        if ([value isNotEmpty]) {
            return value;
        }
    }

    return nil;
}

- (IBAction)showContactPicker:(id)sender {
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    picker.peoplePickerDelegate = self;

    [self presentViewController:picker animated:YES completion:nil];
}

- (IBAction)callBank:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:concat(@"telprompt://", bankContactNumber)]];
}

- (IBAction)sendSmsToBank:(id)sender {
    MFMessageComposeViewController *smsComposeViewController = [[MFMessageComposeViewController alloc] init];
    smsComposeViewController.recipients = @[bankContactNumber];
    smsComposeViewController.messageComposeDelegate = self;

    [self presentViewController:smsComposeViewController animated:YES completion:nil];
}

- (IBAction)sendEmailToBank:(id)sender {
    MFMailComposeViewController *mailComposeViewController = [[MFMailComposeViewController alloc] init];
    [mailComposeViewController setToRecipients:@[bankContactEmail]];
    mailComposeViewController.mailComposeDelegate = self;

    [self presentViewController:mailComposeViewController animated:YES completion:nil];
}

- (IBAction)openHomePage:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:bankHomepage]];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result {

    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error {

    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark ABPeoplePickerNavigationControllerDelegate

- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker
      shouldContinueAfterSelectingPerson:(ABRecordRef)person {

    _card.bankContactId = [NSNumber numberWithInt:ABRecordGetRecordID(person)];
    [cardDao addOrUpdateCard:_card andDo:^(BOOL b, NSString *string, id o) {
    }];

    [self dismissViewControllerAnimated:YES completion:nil];

    return NO;
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker
      shouldContinueAfterSelectingPerson:(ABRecordRef)person
                                property:(ABPropertyID)property
                              identifier:(ABMultiValueIdentifier)identifier {

    [self dismissViewControllerAnimated:YES completion:nil];
    return NO;
}

@end
