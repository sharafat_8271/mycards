//
// Created by Sharafat Ibn Mollah Mosharraf on 5/26/14.
// Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import <MessageUI/MessageUI.h>
#import <Foundation/Foundation.h>
#import <AddressBookUI/ABPeoplePickerNavigationController.h>

@class Card;


@interface ContactBankViewController : UIViewController <ABPeoplePickerNavigationControllerDelegate,
        MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>

@property(assign, nonatomic) Card *card;

@property(weak, nonatomic) IBOutlet UIView *contactBankView;
@property(weak, nonatomic) IBOutlet UIView *pickContactView;

@property(weak, nonatomic) IBOutlet UIButton *callBankButton;
@property(weak, nonatomic) IBOutlet UIButton *sendSmsButton;
@property(weak, nonatomic) IBOutlet UIButton *sendEmailButton;
@property(weak, nonatomic) IBOutlet UIButton *openHomepageButton;

- (IBAction)showContactPicker:(id)sender;

- (IBAction)callBank:(id)sender;

- (IBAction)sendSmsToBank:(id)sender;

- (IBAction)sendEmailToBank:(id)sender;

- (IBAction)openHomePage:(id)sender;

@end
