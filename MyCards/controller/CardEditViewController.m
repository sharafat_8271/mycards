//
// Created by Sharafat Ibn Mollah Mosharraf on 5/22/14.
// Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import "CardEditViewController.h"
#import "CustomUITextField.h"
#import "Card.h"
#import "NSObject+NSObjectExtension.h"
#import "NSString+FormValidation.h"
#import "CardDao.h"
#import "Objection.h"
#import "ObjectionExtension.h"
#import "CMPopTipView.h"


@interface CardEditViewController () {
    CardDao *cardDao;
}
@end


@implementation CardEditViewController

- (void)setCard:(Card *)card {
    _card = card;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    cardDao = objection_inject(CardDao);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)configureView {
    if (_card != nil) {
        self.title = @"Edit Card";

        _issuerBankInput.text = _card.issuerBank;
        _cardNumberInput.text = _card.cardNumber;
        _cardHolderNameInput.text = _card.cardHolderName;

        if ([_card.validFromMonth isNotEmpty]) {
            _validFromMonthInput.text = _card.validFromMonth;
            _validFromYearInput.text = _card.validFromYear;
        }

        if ([_card.validUntilMonth isNotEmpty]) {
            _validUntilMonthInput.text = _card.validUntilMonth;
            _validUntilYearInput.text = _card.validUntilYear;
        }

        if ([_card.cvv isNotEmpty]) {
            _cvvInput.text = _card.cvv;
        }

        NSString *cardImageName = @"";
        switch (_card.cardType) {
            case CardTypeVisa:
                cardImageName = @"visa";
                break;
            case CardTypeMaster:
                cardImageName = @"master";
                break;
            case CardTypeAmericanExpress:
                cardImageName = @"amex";
                break;
            case CardTypeOther:
                break;
        }
        _cardTypeImage.image = [UIImage imageNamed:cardImageName];
    }

    _issuerBankInput.delegate = self;
    _issuerBankInput.leftInsetIcon = [UIImage imageNamed:@"bank"];

    _cardNumberInput.delegate = self;
    _cardNumberInput.leftInsetIcon = [UIImage imageNamed:@"number"];

    _cardHolderNameInput.delegate = self;
    _cardHolderNameInput.leftInsetIcon = [UIImage imageNamed:@"person"];

    _cvvInput.delegate = self;
}

- (IBAction)onSaveButtonClick:(UIBarButtonItem *)sender {
    if (![self validateForm]) {
        return;
    }

    BOOL newCard = _card == nil;
    if (newCard) {
        _card = [[Card alloc] init];
    }

    _card.issuerBank = _issuerBankInput.text;
    _card.cardNumber = _cardNumberInput.text;
    _card.cardHolderName = _cardHolderNameInput.text;
    _card.validFromMonth = _validFromMonthInput.text;
    _card.validFromYear = _validFromYearInput.text;
    _card.validUntilMonth = _validUntilMonthInput.text;
    _card.validUntilYear = _validUntilYearInput.text;
    _card.cvv = _cvvInput.text;

    UINavigationController *navigationController = self.navigationController;

    [cardDao addOrUpdateCard:_card andDo:^(BOOL success, NSString *errorMsg, id returnObject) {
        if (!success) {
            NSLog(@"Error on saving card: %@", errorMsg);

            [[[UIAlertView alloc] initWithTitle:@"Error"
                                        message:errorMsg
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
        }

        [navigationController popViewControllerAnimated:YES];
    }];
}

- (BOOL)validateForm {
    BOOL errorOccurred = false;
    CMPopTipView *popTipView;

    self.issuerBankInput.rightInsetIcon = nil;
    self.cardNumberInput.rightInsetIcon = nil;
    self.cardHolderNameInput.rightInsetIcon = nil;

    if (![_issuerBankInput.text isValidBankName]) {
        self.issuerBankInput.rightInsetIcon = [UIImage imageNamed:@"Error"];
        popTipView = [self popTipViewForTextField:self.issuerBankInput
                                      withMessage:@"Please enter valid Bank name"];
        errorOccurred = true;
    }

    if (![_cardNumberInput.text isValidCardNumber]) {
        self.cardNumberInput.rightInsetIcon = [UIImage imageNamed:@"Error"];

        if (popTipView == nil) {
            popTipView = [self popTipViewForTextField:self.cardNumberInput
                                          withMessage:@"Please enter valid card number"];
        }

        errorOccurred = true;
    }

    if (![_cardHolderNameInput.text isValidCardHolderName]) {
        self.cardHolderNameInput.rightInsetIcon = [UIImage imageNamed:@"Error"];
        if (popTipView == nil) {
            popTipView = [self popTipViewForTextField:self.cardHolderNameInput
                                          withMessage:@"Please enter proper card holder name"];
        }

        errorOccurred = true;
    }

    if (_validFromMonthInput != nil && ![_validFromMonthInput.text isValidMonth]) {
        if (popTipView == nil) {
            popTipView = [self popTipViewForTextField:self.validFromMonthInput
                                          withMessage:@"Please give proper month number"];
        }

        errorOccurred = true;
    }

    if (_validUntilMonthInput != nil && ![_validUntilMonthInput.text isValidMonth]) {
        if (popTipView == nil) {
            popTipView = [self popTipViewForTextField:self.validUntilMonthInput
                                          withMessage:@"Please give proper month number"];
        }

        errorOccurred = true;
    }

    if (_validFromYearInput != nil && ![_validFromYearInput.text isValidYear]) {
        if (popTipView == nil) {
            popTipView = [self popTipViewForTextField:self.validFromYearInput
                                          withMessage:@"Please give proper year number"];
        }

        errorOccurred = true;
    }

    if (_validUntilYearInput != nil && ![_validUntilYearInput.text isValidYear]) {
        if (popTipView == nil) {
            popTipView = [self popTipViewForTextField:self.validUntilYearInput
                                          withMessage:@"Please give proper year number"];
        }

        errorOccurred = true;
    }
    else if (_validFromYearInput != nil && _validUntilYearInput != nil &&
            ![_validFromYearInput.text isEarlierYear:_validUntilYearInput.text]) {
        if (popTipView == nil) {
            popTipView = [self popTipViewForTextField:self.validFromYearInput
                                          withMessage:@"From year cannot be later than the until year"];
        }
        errorOccurred = true;
    }

    return !errorOccurred;
}

- (CMPopTipView *)popTipViewForTextField:(UITextField *)textField withMessage:(NSString *)message {
    CMPopTipView *popTipView;

    popTipView = [[CMPopTipView alloc] initWithMessage:message];
    popTipView.textColor = [UIColor colorWithRed:(CGFloat) (169 / 255.0)
                                           green:(CGFloat) (68 / 255.0)
                                            blue:(CGFloat) (66 / 255.0)
                                           alpha:1];
    popTipView.borderColor = popTipView.textColor;
    popTipView.backgroundColor = [UIColor colorWithRed:(CGFloat) (242 / 255.0)
                                                 green:(CGFloat) (222 / 255.0)
                                                  blue:(CGFloat) (222 / 255.0)
                                                 alpha:1];
    popTipView.hasGradientBackground = NO;

    [popTipView presentPointingAtView:textField inView:self.view animated:YES];

    return popTipView;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return [(CustomUITextField *) textField shouldChangeCharactersInRange:range replacementString:string];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    return [(CustomUITextField *) textField shouldReturn];
}

/**
* Hide keyboard when focus is out from UITextField, i.e., the view is touched.
* http://code.tutsplus.com/tutorials/ios-sdk-uitextfield-uitextfielddelegate--mobile-10943#highlighter_480399
*/
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

@end
