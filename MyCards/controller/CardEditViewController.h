//
// Created by Sharafat Ibn Mollah Mosharraf on 5/22/14.
// Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CustomUITextField;
@class Card;


@interface CardEditViewController : UIViewController <UITextFieldDelegate>

@property(strong, nonatomic) Card *card;

@property(weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property(weak, nonatomic) IBOutlet CustomUITextField *issuerBankInput;
@property(weak, nonatomic) IBOutlet CustomUITextField *cardNumberInput;
@property(weak, nonatomic) IBOutlet CustomUITextField *cardHolderNameInput;
@property(weak, nonatomic) IBOutlet CustomUITextField *validFromMonthInput;
@property(weak, nonatomic) IBOutlet CustomUITextField *validFromYearInput;
@property(weak, nonatomic) IBOutlet CustomUITextField *validUntilMonthInput;
@property(weak, nonatomic) IBOutlet CustomUITextField *validUntilYearInput;
@property(weak, nonatomic) IBOutlet CustomUITextField *cvvInput;
@property(weak, nonatomic) IBOutlet UIImageView *cardTypeImage;

- (IBAction)onSaveButtonClick:(UIBarButtonItem *)sender;

@end
