//
//  CardDetailViewController.h
//  MyCards
//
//  Created by Sharafat Ibn Mollah Mosharraf on 5/21/14.
//  Copyright (c) 2014 Therap (BD) Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Card;

@interface CardDetailViewController : UIViewController <UIAlertViewDelegate>

@property(assign, nonatomic) Card *card;

@property(weak, nonatomic) IBOutlet UILabel *issuerBankLabel;
@property(weak, nonatomic) IBOutlet UILabel *cardNumberLabel;
@property(weak, nonatomic) IBOutlet UILabel *validFromLabel;
@property(weak, nonatomic) IBOutlet UILabel *validUntilLabel;
@property(weak, nonatomic) IBOutlet UILabel *cvvLabel;
@property(weak, nonatomic) IBOutlet UILabel *cardHolderNameLabel;
@property(weak, nonatomic) IBOutlet UIImageView *cardTypeImage;

@property(weak, nonatomic) IBOutlet UIView *validFromView;
@property(weak, nonatomic) IBOutlet UIView *validUntilView;
@property(weak, nonatomic) IBOutlet UIView *cvvView;

@end
